package com.example.felix.hadoop;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

class SimpleExtraction implements EmailExtraction
{

    @Override
    public Map<String, Integer> extract(String content)
    {
        String property = System.getProperty("relevant.keywords", "ORCL");
        final Set<String> keywords = Arrays.stream(property.split(";")).collect(Collectors.toSet());


        Map<String, Integer> map = new HashMap<>();
        StringTokenizer tokenizer = new StringTokenizer(content);

        while (tokenizer.hasMoreTokens())
        {
            String token = tokenizer.nextToken();
            if (keywords.contains(token))
                map.compute(token, (s, integer) ->
                {
                    if (integer == null)
                    {
                        return 1;
                    }
                    return integer + 1;
                });
        }

        return map;
    }
}
