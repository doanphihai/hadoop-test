package com.example.felix.hadoop;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.lang.StringUtils;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

class MailProcessor
{

    private final Session mailSession;
    private final EmailExtraction extraction;

    MailProcessor(EmailExtraction extraction)
    {
        Properties props = System.getProperties();
        props.put("mail.host", "smtp.dummydomain.com");
        props.put("mail.transport.protocol", "smtp");

        mailSession = Session.getDefaultInstance(props, null);

        this.extraction = extraction;
    }


    EmailData process(InputStream source)
    {
        try
        {
            MimeMessage message = new MimeMessage(mailSession, source);

            final String from = Arrays.stream(message.getFrom()).map(Address::toString).reduce((a, b) -> a + ";" + b).orElse("");

            final Map<String, Integer> extract = extraction.extract(message.getContent().toString());

            final Set<String> recipients = Arrays.stream(message.getAllRecipients()).map(Address::toString).collect(Collectors.toSet());

            final String date = message.getReceivedDate().toString();

            final String content = processContent(message);

            return new EmailData(message.getSubject(), from, date, recipients, extract, content);
        }
        catch (MessagingException | IOException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    private String processContent(Message message) throws IOException, MessagingException
    {
        Multipart multipart = (Multipart) message.getContent();

        for (int i = 0; i < multipart.getCount(); i++)
        {
            BodyPart bodyPart = multipart.getBodyPart(i);

            if (Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition()) &&
                    StringUtils.isBlank(bodyPart.getFileName()))
            {
                InputStream is = bodyPart.getInputStream(); // dealing with non attachments only
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] buf = new byte[4096];
                while ((is.read(buf, 0, buf.length)) >= 0)
                {
                    byteArrayOutputStream.write(buf);
                }
                String result = byteArrayOutputStream.toString();
                byteArrayOutputStream.close();
                return result;
            }

            //TODO: attachments can be processed here - OUT OF SCOPE
        }
        return null;
    }

    @Data
    @AllArgsConstructor
    static class EmailData
    {
        private String subject;
        private String from;
        private String date;
        private Set<String> recipients;
        private Map<String, Integer> posList;
        private String content;
    }
}
