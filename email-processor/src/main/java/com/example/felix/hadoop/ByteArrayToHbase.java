package com.example.felix.hadoop;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableOutputFormat;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Objects;

import static com.example.felix.hadoop.Constants.BYTE_ARRAY_TO_HBASE_TABLE;
import static com.example.felix.hadoop.Constants.EXTRACTION_CF;
import static com.example.felix.hadoop.Constants.ORIGIN_CF;
import static com.example.felix.hadoop.Constants.Q_CONTENT;
import static com.example.felix.hadoop.Constants.Q_DATE;
import static com.example.felix.hadoop.Constants.Q_EMAIL_ID;
import static com.example.felix.hadoop.Constants.Q_RECIPIENTS;
import static com.example.felix.hadoop.Constants.Q_SUBJECT;

public class ByteArrayToHbase
{

    private static class ByteArrayToHbaseMapper extends Mapper<Text, BytesWritable, ImmutableBytesWritable, Writable>
    {
        final static Logger LOG = LoggerFactory.getLogger(ByteArrayToHbaseMapper.class);

        private MailProcessor mailProcessor = null;

        @Override
        protected void setup(Context context) throws IOException, InterruptedException
        {
            mailProcessor = Utilities.getClassInstanceFromProperty(MailProcessor.class,
                    "extraction.class",
                    "com.example.felix.hadoop.SimpleExtraction");

            super.setup(context);
        }

        @Override
        protected void map(Text key, BytesWritable value, Context context)
        {
            byte[] bytes = value.getBytes();
            final ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
            final MailProcessor.EmailData emailData = mailProcessor.process(inputStream);

            try
            {
                context.write(new ImmutableBytesWritable(key.getBytes()), (Writable) mapToPutObject(key.toString(), emailData));
            }
            catch (IOException | InterruptedException e)
            {
                e.printStackTrace();
            }

            LOG.info("data inserted successfully");
        }

        private Put mapToPutObject(String rowKey, MailProcessor.EmailData emailData)
        {
            // instantiate Put class
            final Put putExtractionData = new Put(Bytes.toBytes(rowKey));

            emailData.getPosList().forEach((key, value) ->
            {
                //Add to "extraction" column family
                putExtractionData.addImmutable(EXTRACTION_CF,
                        Q_EMAIL_ID, Bytes.toBytes(emailData.getFrom()));

                //we flat them out by using prefix
                putExtractionData.addImmutable(EXTRACTION_CF, Bytes.toBytes("pos@" + key),
                        Bytes.toBytes(value));
            });

            //Add to "origin" column family
            putExtractionData.addImmutable(ORIGIN_CF,
                    Q_DATE, Bytes.toBytes(emailData.getDate()));
            putExtractionData.addImmutable(ORIGIN_CF,
                    Q_SUBJECT, Bytes.toBytes(emailData.getSubject()));
            putExtractionData.addImmutable(ORIGIN_CF,
                    Q_RECIPIENTS,
                    Bytes.toBytes(String.join(";", emailData.getRecipients())));
            putExtractionData.addImmutable(ORIGIN_CF,
                    Q_CONTENT,
                    Bytes.toBytes(emailData.getContent()));

            return putExtractionData;

        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException, ServiceException
    {
        Configuration config = HBaseConfiguration.create();
        String path = Objects.requireNonNull(ByteArrayToHbase.class
                .getClassLoader()
                .getResource("hbase-site.xml"))
                .getPath();
        config.addResource(new Path(path));
        //At this point, if HBase is not available then main method throws an exception and exit gracefully
        HBaseAdmin.checkHBaseAvailable(config);

        try (Connection connection = ConnectionFactory.createConnection(config))
        {
            Utilities.createTable(connection, BYTE_ARRAY_TO_HBASE_TABLE, "origin", "extraction");

            final Job job = Job.getInstance(config, "FromHadoopToHBase");

            job.setJarByClass(ByteArrayToHbase.class);
            job.setMapperClass(ByteArrayToHbaseMapper.class);
            job.setOutputFormatClass(TableOutputFormat.class);
            job.getConfiguration().set(TableOutputFormat.OUTPUT_TABLE, BYTE_ARRAY_TO_HBASE_TABLE);
            job.setOutputKeyClass(ImmutableBytesWritable.class);
            job.setOutputValueClass(Writable.class);
            job.setNumReduceTasks(0);
            FileInputFormat.addInputPath(job, new Path(args[0]));

            System.exit(job.waitForCompletion(true) ? 0 : 1);
        }
    }

}
