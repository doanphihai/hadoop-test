package com.example.felix.hadoop;

import java.util.Map;

public interface EmailExtraction
{
    Map<String, Integer> extract(String data);
}
