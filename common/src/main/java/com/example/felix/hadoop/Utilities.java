package com.example.felix.hadoop;

import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;

import java.io.IOException;

public class Utilities
{
    static void createTable(Connection connection, String tableName, String... cfs) throws IOException
    {
        //get admin
        Admin admin = connection.getAdmin();

        if (!admin.tableExists(TableName.valueOf(tableName)))
        {
            HTableDescriptor desc = new HTableDescriptor(TableName.valueOf(tableName));
            for (String name: cfs)
            {
                desc.addFamily(new HColumnDescriptor(name));
            }
            admin.createTable(desc);
        }
    }

    static <T> T getClassInstanceFromProperty(Class<T> clazz, String propKey, String defValue) throws IOException
    {
        final String property = System.getProperty(propKey, defValue);
        try
        {
            return (T) clazz.getClassLoader().loadClass(property).newInstance();
        }
        catch (IllegalAccessException | InstantiationException | ClassNotFoundException e)
        {
            e.printStackTrace();
            throw new IOException("Cannot load class " + property, e);
        }
    }
}
