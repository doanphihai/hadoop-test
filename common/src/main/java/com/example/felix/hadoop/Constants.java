package com.example.felix.hadoop;

import org.apache.hadoop.hbase.util.Bytes;

public interface Constants
{
    byte[] ORIGIN_CF = Bytes.toBytes("origin");
    byte[] EXTRACTION_CF = Bytes.toBytes("extraction");
    byte[] GENERAL_CF = Bytes.toBytes("general");
    byte[] Q_EMAIL_ID = Bytes.toBytes("email_id");
    byte[] Q_DATE = Bytes.toBytes("date");
    byte[] Q_SUBJECT = Bytes.toBytes("subject");
    byte[] Q_CONTENT = Bytes.toBytes("content");
    byte[] Q_RECIPIENTS = Bytes.toBytes("recipients");
    String BYTE_ARRAY_TO_HBASE_TABLE = "processed_emails";
    String PRIMARY_FEATURES_TABLE = "primary_features";
    String FINAL_TABLE = "final_features";
}
