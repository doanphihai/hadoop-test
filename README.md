##### Project's structure

###### common

The common project contains utility classes and reused codes

###### uri-to-hadoop-file-sequence

The project load ELM files from given URIs and write them to Hadoop sequence file ( which has key as the URI ). It needs an input file which contains all the ELM file URIs from local directory.

The output of this process is going to be the input of the next one. 

###### email-processor

The email-processor will read from Hadoop the ELM content and parse it using javax-mail library. It also extracts all necessary information which are inputs for the next phases.
The following information are the key-value(s) which are written to HBase table

Table name: `processed_emails`

Key: `URI of file`

Value: `email_id, date, subject, recipients, content, pos@<keyword1>, ...pos@<keywordN>`

This process also accepts a JVM parameter as a full class name to an implementation of extraction interface, 
by default it will use `com.example.felix.hadoop.SimpleExtraction` to extract email information. 
Any new implementation should implement `interface EmailExtraction` to comply with the requirement.

An example of command line: `hadoop jar email-processor-0.1.jar ByteArrayToHbase 
-Dmapreduce.map.java.opts="-Dextraction.class=com.example.felix.hadoop.SimpleExtraction" /app_in `

###### primary-feature-calculation

This process is a mapper which transforms data of processed_emails table to primary_features table on HBase. 
This also provides some calculations to consolidate the data.
 
Table name: `primary_features`

Key: `URI of file`

Value: `date, content_length, <keyword1>@relevant, ...<keywordN>@relevant`

This process accepts a JVM parameter as a full class name to an implementation of calculation interface, 
by default it will use `com.example.felix.hadoop.SimpleFeaturesCalculation` to extract email information. 
Any new implementation should implement `interface FeaturesCalculation` to comply with the requirement.

An example of command line: `hadoop jar primary-feature-calculation-0.1.jar MappingDataFromExtractionTable 
-Dmapreduce.map.java.opts="-Dcalculation.class=com.example.felix.hadoop.SimpleFeaturesCalculation" `

###### second-feature-calculation

The last process is a mapper-reducer process. This reads from HBase table ( primary_features ) 
then map the data to key-value pairs `(<email's sent time>, <relevant@keywordX>), (<email's sent time>, <content_length>)`
 
 
The reducer will pick up the key-values and aggregate them by calculating their average values and write them to HBase `final_features` table.

Table name: `final_features`

Key: `Email's sent time`

Value: `average content_length, average <keyword1>@relevant, ...average <keywordN>@relevant`
