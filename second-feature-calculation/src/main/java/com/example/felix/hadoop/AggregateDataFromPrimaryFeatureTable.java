package com.example.felix.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.stream.StreamSupport;

import static com.example.felix.hadoop.Constants.FINAL_TABLE;
import static com.example.felix.hadoop.Constants.PRIMARY_FEATURES_TABLE;

public class AggregateDataFromPrimaryFeatureTable
{

    private static class SecondFeatureTableMapper extends TableMapper<Text, DoubleWritable>
    {
        @Override
        protected void map(ImmutableBytesWritable key, Result value, Context context) throws IOException, InterruptedException
        {
            long fromEpochMill = Bytes.toLong(value.getValue(Constants.GENERAL_CF, Constants.Q_DATE));
            final ZonedDateTime dateTime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(fromEpochMill), ZoneId.of("UTC"));
            for (KeyValue kv : value.raw())
            {
                String q = Bytes.toString(kv.getQualifier());
                if (q.contains("@relevant"))
                {
                    context.write(new Text(q + ":" + getKey(dateTime)), new DoubleWritable(Bytes.toDouble(kv.getValue())));
                }
                else if (q.contains("content_length"))
                {
                    context.write(new Text(q + ":" + getKey(dateTime)), new DoubleWritable(Bytes.toDouble(kv.getValue())));
                }
            }
        }

        private int getKey(ZonedDateTime dateTime)
        {
            return dateTime.getHour() * 3600 + dateTime.getMinute() * 60;
        }
    }

    private static class SecondFeatureTableReducer extends TableReducer<Text, DoubleWritable, ImmutableBytesWritable>
    {
        @Override
        protected void reduce(Text key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException
        {
            String keyInString = key.toString();
            if (keyInString.contains("@relevant"))
            {
                final String relevantField = keyInString.split(":")[0];
                final String keyInHourMinute = keyInString.split(":")[1];

                final double average = StreamSupport.stream(values.spliterator(), false).mapToDouble(DoubleWritable::get).average().orElse(0);
                Put p = new Put(Bytes.toBytes(keyInHourMinute));
                p.addImmutable(Constants.GENERAL_CF, Bytes.toBytes(relevantField), Bytes.toBytes(average));

                context.write(null, p);
            }
            else if (keyInString.contains("content_length"))
            {
                final String relevantField = keyInString.split(":")[0];
                final String keyInHourMinute = keyInString.split(":")[1];

                final double average = StreamSupport.stream(values.spliterator(), false).mapToDouble(DoubleWritable::get).average().orElse(0);
                Put p = new Put(Bytes.toBytes(keyInHourMinute));
                p.addImmutable(Constants.GENERAL_CF, Bytes.toBytes(relevantField), Bytes.toBytes(average));

                context.write(null, p);
            }
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException
    {
        Configuration config = HBaseConfiguration.create();
        Job job = Job.getInstance(config, "AggregationEverything");
        job.setJarByClass(AggregateDataFromPrimaryFeatureTable.class);     // class that contains mapper and reducer

        Scan scan = new Scan();
        scan.setCaching(500);
        scan.setCacheBlocks(false);

        TableMapReduceUtil.initTableMapperJob(
                PRIMARY_FEATURES_TABLE,        // input table
                scan,               // Scan instance to control CF and attribute selection
                SecondFeatureTableMapper.class,     // mapper class
                Text.class,         // mapper output key
                IntWritable.class,  // mapper output value
                job);
        TableMapReduceUtil.initTableReducerJob(
                FINAL_TABLE,        // output table
                SecondFeatureTableReducer.class,    // reducer class
                job);
        job.setNumReduceTasks(1);   // at least one, adjust as required

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
