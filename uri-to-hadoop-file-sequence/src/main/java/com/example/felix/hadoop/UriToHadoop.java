package com.example.felix.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class UriToHadoop
{
    public class UriToHadoopMapper extends Mapper<Object, Text, Text, BytesWritable>
    {

        private static final int BUFFER_SIZE = 1024 * 1024;

        @Override
        protected void map(Object key, Text value, Context context) throws IOException, InterruptedException
        {
            //key = filename; value = URI in string format
            final URI fileUri = URI.create(value.toString());
            Path path = Paths.get(fileUri);
            byte[] buffer = new byte[BUFFER_SIZE];
            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            try (InputStream inputStream = new FSDataInputStream(Files.newInputStream(path)))
            {
                while (inputStream.read(buffer, 0, buffer.length) >= 0)
                {
                    byteArrayOutputStream.write(buffer);
                }

                //key = URI in string format; value = file content in byte array format
                context.write(value, new BytesWritable(byteArrayOutputStream.toByteArray()));
            }
        }
    }

    public static void main(String[] args) throws Exception
    {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "FileURIToHadoopSeqFile");
        job.setJarByClass(UriToHadoop.class);
        job.setMapperClass(UriToHadoopMapper.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(BytesWritable.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);

        FileInputFormat.addInputPath(job, new org.apache.hadoop.fs.Path(args[0]));
        FileOutputFormat.setOutputPath(job, new org.apache.hadoop.fs.Path(args[1]));

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}


