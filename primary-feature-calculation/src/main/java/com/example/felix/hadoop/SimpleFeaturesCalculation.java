package com.example.felix.hadoop;

import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;

public class SimpleFeaturesCalculation implements FeaturesCalculation
{
    @Override
    public PrimaryFeature calculate(EmailExtractionData input)
    {
        final Map<String, Double> map = input.getMapPos().keySet().stream()
                .map(s -> new AbstractMap.SimpleEntry<>(s, 0.5))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        return new PrimaryFeature(convertToZonedDateTime(input), input.getContent().length(), map);
    }

    private ZonedDateTime convertToZonedDateTime(EmailExtractionData input)
    {
        ZonedDateTime date = null;
        try
        {
            date = ZonedDateTime.parse(input.getDate());
        }
        catch (DateTimeParseException ignored)
        {

        }
        return date;
    }
}
