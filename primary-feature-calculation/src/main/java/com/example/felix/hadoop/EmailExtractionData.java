package com.example.felix.hadoop;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
class EmailExtractionData
{
    private String subject;
    private String content;
    private String date;
    private List<String> recipients;
    private Map<String, Integer> mapPos;
}
