package com.example.felix.hadoop;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.Map;

@AllArgsConstructor
@Data
class PrimaryFeature
{
    private ZonedDateTime date;
    private int emailLength;
    private Map<String, Double> relevantMap;
}
