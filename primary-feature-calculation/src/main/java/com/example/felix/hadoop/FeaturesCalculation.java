package com.example.felix.hadoop;

public interface FeaturesCalculation
{
    PrimaryFeature calculate(EmailExtractionData input);
}
