package com.example.felix.hadoop;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.example.felix.hadoop.Constants.BYTE_ARRAY_TO_HBASE_TABLE;
import static com.example.felix.hadoop.Constants.EXTRACTION_CF;
import static com.example.felix.hadoop.Constants.ORIGIN_CF;
import static com.example.felix.hadoop.Constants.PRIMARY_FEATURES_TABLE;
import static com.example.felix.hadoop.Constants.Q_CONTENT;
import static com.example.felix.hadoop.Constants.Q_DATE;
import static com.example.felix.hadoop.Constants.Q_RECIPIENTS;
import static com.example.felix.hadoop.Constants.Q_SUBJECT;

public class MappingDataFromExtractionTable
{
    private static class CalculateDataFromHBaseMapper extends TableMapper<ImmutableBytesWritable, Put>
    {
        private final static Logger LOG = LoggerFactory.getLogger(CalculateDataFromHBaseMapper.class);

        //hard code - but it should read from JVM parameters
        private List<String> keywords = Arrays.asList("ORCL", "IBM", "APPL");

        private FeaturesCalculation processor;

        @Override
        protected void setup(Context context) throws IOException, InterruptedException
        {
            //Read from -Dcalculation.class
            processor = Utilities.getClassInstanceFromProperty(FeaturesCalculation.class,
                    "calculation.class",
                    "com.example.felix.hadoop.SimpleFeaturesCalculation");

            super.setup(context);
        }

        @Override
        public void map(ImmutableBytesWritable row, Result value, Context context) throws IOException, InterruptedException
        {
            final String date = Bytes.toString(value.getValue(ORIGIN_CF, Q_DATE));
            final String subject = Bytes.toString(value.getValue(ORIGIN_CF, Q_SUBJECT));
            final String content = Bytes.toString(value.getValue(ORIGIN_CF, Q_CONTENT));

            final Map<String, Integer> mapOfPos = keywords.stream()
                    .filter(s -> value.containsColumn(EXTRACTION_CF, Bytes.toBytes("pos@" + s)))
                    .map(str -> new AbstractMap.SimpleEntry<>(str, Bytes.toInt(value.getValue(EXTRACTION_CF, Bytes.toBytes("pos@" + str)))))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

            final List<String> recipients = Arrays
                    .stream(Bytes.toString(value.getValue(ORIGIN_CF, Q_RECIPIENTS))
                            .split(";")).collect(Collectors.toList());

            final EmailExtractionData extractionData = new EmailExtractionData(subject, content, date, recipients, mapOfPos);

            final PrimaryFeature primaryFeature = processor.calculate(extractionData);

            context.write(row, mapToPutObject(row, primaryFeature));
        }

        private Put mapToPutObject(ImmutableBytesWritable row, PrimaryFeature feature)
        {

            final Put put = new Put(row.get());

            put.addImmutable(Constants.GENERAL_CF, Bytes.toBytes("date"), Bytes.toBytes(feature.getDate().toInstant().toEpochMilli()));
            put.addImmutable(Constants.GENERAL_CF, Bytes.toBytes("content_length"), Bytes.toBytes(feature.getEmailLength()));

            feature.getRelevantMap().forEach((s, value) ->
                    put.addImmutable(Constants.GENERAL_CF, Bytes.toBytes(s + "@relevant"), Bytes.toBytes(value)));

            return put;
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException, ServiceException
    {
        Configuration config = HBaseConfiguration.create();
        String path = Objects.requireNonNull(MappingDataFromExtractionTable.class
                .getClassLoader()
                .getResource("hbase-site.xml"))
                .getPath();
        config.addResource(new Path(path));

        //At this point, if HBase is not available then main method throws an exception and exit gracefully
        HBaseAdmin.checkHBaseAvailable(config);

        try (Connection connection = ConnectionFactory.createConnection(config))
        {
            Utilities.createTable(connection, PRIMARY_FEATURES_TABLE, "general");

            Job job = Job.getInstance(config, "MappingDataFromExtractionTable");
            job.setJarByClass(MappingDataFromExtractionTable.class);

            Scan scan = new Scan();
            scan.setCaching(500); //is there any better numbers?
            scan.setCacheBlocks(false);

            TableMapReduceUtil.initTableMapperJob(
                    TableName.valueOf(BYTE_ARRAY_TO_HBASE_TABLE),        // input table
                    scan,               // Scan instance to control CF and attribute selection
                    CalculateDataFromHBaseMapper.class,     // mapper class
                    Text.class,         // mapper output key
                    IntWritable.class,  // mapper output value
                    job);

            System.exit(job.waitForCompletion(true) ? 0 : 1);
        }
    }
}
